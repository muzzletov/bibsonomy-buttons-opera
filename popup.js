var bgPage = chrome.extension.getBackgroundPage();
var openHome = null;
var savePost = null;
var getCheckboxes = null;

function setOption(e) {localStorage[e.target.name] = e.target.checked;}

var openHome = function () {
	bgPage.openHome();
	window.close();
}

var saveBookmark = function () {
	bgPage.savePost(true);
	window.close();
}

var savePublication = function () {
	bgPage.savePost(false);
	window.close();
}


document.addEventListener('DOMContentLoaded', 
	function () {
		var optionsArea = document.getElementById("optionsArea");
		var cogwheel = document.getElementById("cogwheel");
		var homeButton = document.getElementById("homeButton");
		var saveButton = document.getElementById("saveButton");
		var bookmarkButton = document.getElementById("bookmarkButton")
		var publicationButton = document.getElementById("publicationButton");
		var optionsButton =	document.getElementById("optionsButton");
		var controlArea = document.getElementById("controlArea");
		var locales = document.getElementsByTagName("localestring");
		var checkboxes = document.querySelectorAll(".checkBox");
		var shortcutInputFields = new Array("shortcutHome","shortcutBookmark","shortcutPublication");
		var controlElements = document.getElementsByClassName("control");
		var element;
		
		getCheckboxes = function(){return checkboxes;}
		
		reflectStoredSettings();
		
		for(var i = 0; locales.length > i; i++) 
			locales[i].innerHTML=chrome.i18n.getMessage(locales[i].innerHTML);
		
		optionsArea.style.display = "none";
		cogwheel.style.display = "none";

		homeButton.title = chrome.i18n.getMessage("homeButtonTitle");
		publicationButton.title = chrome.i18n.getMessage("publicationButtonTitle");
		bookmarkButton.title = chrome.i18n.getMessage("bookmarkButtonTitle");
		saveButton.value = chrome.i18n.getMessage("saveButtonLabel");
		optionsButton.title = chrome.i18n.getMessage("showOptionsLabel");

		for(var i = 0; i < shortcutInputFields.length; i++) {
			element = document.getElementById(shortcutInputFields[i]);
			element.onkeydown = keydownGrabber;
			element.onkeyup = keyupGrabber;
			element.value = localStorage[shortcutInputFields[i]]!=undefined?localStorage[shortcutInputFields[i]]:"";
		}
		
		saveButton.addEventListener('click', saveOptions);
		homeButton.addEventListener('click', openHome);
		bookmarkButton.addEventListener('click', saveBookmark);
		publicationButton.addEventListener('click', savePublication);
		optionsButton.addEventListener('click', 
				function(e){
					var visibility = "none";
					var indicator = "cogwheel";
					var optionsButtonClass = "spacer";
					
					optionsButton.title = chrome.i18n.getMessage("showOptionsLabel");
					
					if(optionsArea.style.display=="none") {
						visibility="";
						indicator="contract";
						optionsButtonClass="";
						reflectStoredSettings();
						optionsButton.title = chrome.i18n.getMessage("hideOptionsLabel");
					}
					controlArea.style.display=optionsArea.style.display;
					optionsArea.style.display=visibility;
					cogwheel.style.display=visibility;
					e.target.src="images/"+indicator+".png";
					e.target.className=optionsButtonClass;
				}
		);
	}
);

function reflectStoredSettings() {
	var appendTheseInput = document.getElementById("alwaysAppendThese");
	var checkboxes = getCheckboxes();
	for(var i = 0; checkboxes.length > i; i++) {
		checkboxes[i].checked=false;
		if(localStorage[checkboxes[i].name]=="true")
			checkboxes[i].checked=true;
	}
	if((appendTheseInput.value=localStorage[appendTheseInput.name])==undefined)
		appendTheseInput.value="";
	
}

function saveOptions() {
	var appendTheseInput = document.getElementById("alwaysAppendThese");
	var checkboxes = getCheckboxes();
	for(var i = 0; checkboxes.length > i; i++) {
		localStorage[checkboxes[i].name]="false";
		if(checkboxes[i].checked)
			localStorage[checkboxes[i].name]="true";
	}
	localStorage[appendTheseInput.name]=appendTheseInput.value;
	window.close();
}
