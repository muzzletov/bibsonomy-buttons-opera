var getKeyStack = null;
var getSavedKeysStack = null;
var getCurrentInput = null;
var getCurrentCallback = null;

{
	var stack = new Array();
	var stackSavedKeys = new Array();
	var input = null;
	var callback = null;
	getKeyStack = function() {
		return stack;
	};
	getCurrentInput = function(element) {
		if (element != undefined)
			input = element;
		return input;
	};
	getCurrentCallback = function(newCallback) {
		if (newCallback != undefined)
			callback = newCallback;
		return callback;
	};
	getSavedKeyStack = function(value) {
		if (value != undefined)
			stackSavedKeys = value;
		return stackSavedKeys;
	};
}

function getKeyString(keyCode) {
	return keyCode == 17 ? chrome.i18n.getMessage("CTRL")
				: keyCode == 16 ? chrome.i18n.getMessage("SHIFT")
						: keyCode == 8 ? chrome.i18n
								.getMessage("BACKSPACE")
								: keyCode == 9 ? chrome.i18n
										.getMessage("TAB")
										: keyCode == 46 ? chrome.i18n
												.getMessage("DEL")
												: keyCode == 13 ? chrome.i18n
														.getMessage("ENTER")
														: keyCode == 32 ? chrome.i18n
																.getMessage("SPACE")
																: String.fromCharCode(keyCode);
}

function saveShortcut(e) {
	var keys = getKeyStack().length != 0?getKeyStack():getSavedKeyStack();
	e.target.value="";
	for ( var i = 0; i < keys.length; i++)
		e.target.value += getKeyString(keys[i])
				+ (i == keys.length - 1 ? " " : " + ");
	chrome.extension.sendMessage({
		message : e.target.id+"Set",
		shortcut : e.target.value,
		name: e.target.id
	}, function(response) {
		if(!response.success) 
			e.target.value=response.oldValue;
		displayMessage(response.message);
	});
	e.target.blur();
	getSavedKeyStack().length=getKeyStack().length=0;
	return !1;
}

function displayMessage(msg) {
	var infoText = document.getElementById("infoText");
	infoText.innerHTML = msg;
}

function keyupGrabber(e) {
	e.preventDefault();
	var index = -1;

	if ((index = getKeyStack().indexOf(e.keyCode)) != -1) {
		getKeyStack().splice(index, 1);
		if (getKeyStack().length == 0 
				&& getSavedKeyStack().length > 1) {
			e.target.value = "";
			if(15 < getSavedKeyStack()[getSavedKeyStack().length-1] 
			&& getSavedKeyStack()[getSavedKeyStack().length-1] < 18)
				return !1;
			saveShortcut(e);
		}
	}
	return !1;
}

function reset(e) {
	getKeyStack().length = 0;
	e.target.value = "";
	return !1;
}
function keydownGrabber(e) {
	e.preventDefault();
	
	if(e.altKey) 
		reset(e);

	if (getKeyStack().indexOf(e.keyCode) > -1)
		return !1;

	getSavedKeyStack().length = 0;
	
	if(!((64 < e.keyCode && e.keyCode < 91)
			||(15 < e.keyCode && e.keyCode < 18)
			||(96 < e.keyCode && e.keyCode < 123))) {
		displayMessage(chrome.i18n.getMessage("invalidChar"));
		return reset(e);
	}
	
	if ((!getKeyStack().length && !(15 < e.keyCode && e.keyCode < 18))
			|| (getKeyStack().length > 0 && getKeyStack().indexOf(16)
					+ getKeyStack().indexOf(17) == -2)) {
		return reset(e);
	}
	getKeyStack().push(parseInt(e.keyCode));
	
	if((64 < e.keyCode && e.keyCode < 91)
			||(96 < e.keyCode && e.keyCode < 123))
			return saveShortcut(e);
	e.target.value = "";
	for ( var i = 0; i < getKeyStack().length; i++) {
		e.target.value += getKeyString(getKeyStack()[i])
				+ (i == getKeyStack().length - 1 ? " " : " + ");
		getSavedKeyStack()[i] = getKeyStack()[i];
	}
	return !1;
}