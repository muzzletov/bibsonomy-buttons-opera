var PROJECT_URL="http://www.bibsonomy.org/";
var contextTitles = [
	["bookmarkButtonTitle", "selection", "bookmarkContextMenu"], 
	["publicationButtonTitle", "selection", "publicationContextMenu"]
];

function invokeAction(url, tab) {
	if(localStorage.openNewTab=="true")	chrome.tabs.create({'url':url});
	else chrome.tabs.update(tab.id, {'url':url});
}

for(var i = 0; i < contextTitles.length; i++) {
	if(localStorage[contextTitles[i]]!=undefined 
		&& localStorage[contextTitles[i][2]]) {

		chrome.contextMenus.create({
			title: contextTitles[i][0],
			contexts: contextTitles[i][1],
			onclick: savePost(i%2)
		});

	}
}
// umschreiben in sendMessage
function openHome(tab) {chrome.tabs.getSelected(null, function(tab) {invokeAction(PROJECT_URL+'/myBibSonomy', tab);});}
function savePost(saveBookmark) {
	chrome.tabs.getSelected(null, function(tab) {
		chrome.tabs.sendMessage(tab.id, {message: "requestURLData"}, function(response){
			invokeAction(PROJECT_URL+'/'+(saveBookmark?'ShowBookmarkEntry?':'BibtexHandler?requTask=upload&')+'selection='
			+encodeURIComponent(response.selection)
			+'&url='
			+encodeURIComponent(response.url)
			+'&referer='
			+encodeURIComponent(response.url)
			+'&description='
			+encodeURIComponent(response.title)
			+((localStorage["alwaysAppendThese"]=="true" && localStorage["tags"].length > 0)?'&tags='+encodeURIComponent(localStorage["tags"]):""), 
			tab);
		  });
	});
}

function isAlreadyAssignedShortcut(shortcut) {
	var shortcutSettings = new Array("shortcutHome","shortcutBookmark","shortcutPublication");
	for(var i = 0; i < shortcutSettings.length; i++) 
		if(shortcut==localStorage[shortcutSettings[i]])return shortcutSettings[i];
	return false;
}

// Hier hin

chrome.runtime.onMessage.addListener(function(request, i, sendResponse) {
	switch(request.message) {
		case "actionHome":
			if(localStorage.shortcutHomeEnabled=="true") 
				openHome();
			break;
		case "actionBookmark":
		case "actionPublication":
			if("actionBookmark" == request.message) {
				if(localStorage.shortcutBookmarkEnabled=="true") 
					savePost(true);
				break;
			} else if(localStorage.shortcutPublicationEnabled=="true")
				savePost(false);
			break;
			
			
			break;
		case "shortcutHomeSet":
		case "shortcutBookmarkSet":
		case "shortcutPublicationSet":
			var msg=chrome.i18n.getMessage("shortcutSaveFailure");
			var assignedTo=isAlreadyAssignedShortcut(request.shortcut);
			var assignSuccess=(!assignedTo||assignedTo==request.name?true:false);
			if(assignSuccess) { 
				localStorage[request.name]=request.shortcut; 
				msg=chrome.i18n.getMessage("shortcutSaveSuccess"); 
			}
			sendResponse({success:assignSuccess, message: msg, oldValue:localStorage[request.name]!=undefined?localStorage[request.name]:""});
			chrome.windows.getAll({populate: true}, function (windowArray){
				for(var i =0; i < windowArray.length; i++) {
					for(var j =0; j < windowArray[i].tabs.length; j++) {
							chrome.tabs.sendMessage(windowArray[i].tabs[j].id, {message: "updateShortcuts"}, function(){});
					}
				}
			});
			break;
		case "shortcutHome":
		case "shortcutBookmark":
		case "shortcutPublication":
			sendResponse({shortcut:localStorage[request.message]});
			break;
		default: break;
	}
	return true;
});
